package org.firas.analysis

import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.paint.Color

class LeastSquare(val points: Collection<Pair<Double, Double>>) {

    var xMin: Double = 0.0
    var yMin: Double = 0.0
    var xMax: Double = 0.0
    var yMax: Double = 0.0

    init {
        val iterator = points.iterator()
        if (!iterator.hasNext()) {
            throw IllegalArgumentException("No point")
        }
        val first = iterator.next()
        xMin = first.first
        xMax = first.first
        yMin = first.second
        yMax = first.second

        while (iterator.hasNext()) {
            val item = iterator.next()
            if (item.first < xMin) {
                xMin = item.first
            } else if (item.first > xMax) {
                xMax = item.first
            }
            if (item.second < yMin) {
                yMin = item.second
            } else if (item.second > yMax) {
                yMax = item.second
            }
        }
    }

    companion object {
        @JvmStatic
        fun drawGrid(context: GraphicsContext) {
            context.stroke = Color.GRAY
            context.setLineDashes(7.0, 4.0)
            // context.beginPath()
            for (i in 1 .. 10) {
                val j = i * 50.0
                /*
                context.moveTo(50.0 + j, 50.0)
                context.lineTo(50.0 + j, 550.0)
                */

                context.moveTo(50.0, j)
                context.lineTo(550.0, j)

                context.strokeLine(50 + j, 50.0, 50 + j, 550.0)
                context.strokeLine(50.0, j, 550.0, j)
            }
            // context.stroke()
            // context.closePath()
        }

        @JvmStatic
        fun drawCoordinate(context: GraphicsContext) {
            context.stroke = Color.GRAY
            context.strokeLine(50.0, 50.0, 50.0, 550.0)
            context.strokeLine(50.0, 550.0, 550.0, 550.0)
        }
    }

    fun draw(canvas: Canvas): Canvas {
        val context = canvas.graphicsContext2D
        drawCoordinate(context)
        drawGrid(context)
        drawPoints(context)
        drawFittingLine(context)
        return canvas
    }

    val pointWidth = 2
    fun drawPoints(context: GraphicsContext) {
        context.fill = Colors.values[5]
        points.forEach {
            val x = if (xMax <= xMin) 300.0 else 50.0 + (it.first - xMin) * 500 / (xMax - xMin)
            val y = if (yMax <= yMin) 300.0 else 550.0 - (it.second - yMin) * 500 / (yMax - yMin)
            context.fillOval(x - pointWidth, y - pointWidth,
                    2.0 * pointWidth, 2.0 * pointWidth)
        }
    }

    private fun drawFittingLine(context: GraphicsContext) {
        val params = Analysis.leastSquare(points)
        val xAvg = params.first
        val yAvg = params.second
        val tangent = params.third

        val y0 = yAvg + (xMin - xAvg) * tangent
        val y1 = yAvg + (xMax - xAvg) * tangent

        context.stroke = Colors.values[1]
        context.setLineDashes(0.0)
        context.strokeLine(50.0, 550.0 - (y0 - yMin) * 500 / (yMax - yMin),
                550.0, 550.0 - (y1 - yMin) * 500 / (yMax - yMin))
    }
}