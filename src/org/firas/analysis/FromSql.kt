package org.firas.analysis

import java.io.*
import java.sql.*
import java.util.*
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.layout.Pane
import javafx.scene.paint.Color
import javafx.scene.shape.ArcType
import javafx.stage.Stage

class FromSql: javafx.application.Application() {

    override fun start(primaryStage: Stage?) {
        primaryStage?.title = "FCC 201803"
        val root = addCanvas(StackPane())
        primaryStage?.scene = Scene(root, 600.0, 600.0)
        primaryStage?.show()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            /**
            if (args.isEmpty()) {
                System.out.println("Usage: java -jar fcc201803.jar " +
                        "MySql_host MySql_db MySql_user MySql_password " +
                        "sql_file [charset]")
            }
            val sql = readSql(args)
            val connection = getMySqlConnection(args)
            val result = query(connection, sql)

            connection.close()
            **/
            launch(FromSql::class.java)
        }

        @JvmStatic
        fun addCanvas(pane: Pane): Pane {
            val canvas = drawOnCanvas(Canvas(600.0, 600.0))
            pane.children.add(canvas)
            return pane
        }

        fun drawOnCanvas(canvas: Canvas): Canvas {
            // return PieChart(arrayOf(5.0, 4.0, 3.0, 2.0, 1.0)).draw(canvas)
            return LeastSquare(arrayListOf(Pair(1.0, 1.0),
                    Pair(2.0, 2.0),
                    Pair(2.5, 3.0),
                    Pair(4.0, 4.5))).draw(canvas)
        }

        @JvmStatic
        private fun readSql(args: Array<String>): String {
            val scanner = getSqlScanner(args)
            val buffer = StringBuilder()
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine())
                buffer.append(' ')
            }
            return buffer.toString()
        }

        /**
         * @throws FileNotFoundException
         * @throws IOException
         */
        @JvmStatic
        private fun getSqlScanner(args: Array<String>): Scanner {
            return if (args.size >= 6) Scanner(File(args[4]), args[5])
                    else Scanner(File(args[4]))
        }

        /**
         * @throws SQLException
         */
        @JvmStatic
        private fun getMySqlConnection(args: Array<String>): Connection {
            Class.forName("com.mysql.jdbc.Driver")
            return checkNotNull(DriverManager.getConnection("jdbc:mysql://" + args[0] +
                    '/' + args[1] + "?useSSL=false", args[2], args[3]))
        }

        /**
         * @throws SQLException
         */
        @JvmStatic
        private fun query(connection: Connection, sql: String): List<Map<String, Any?>> {
            val statement = connection.createStatement()
            statement.execute(sql)
            val resultSet = statement.resultSet

            val metaData = getMetaData(resultSet.metaData)

            val result = ArrayList<Map<String, Any?>>()
            while (resultSet.next()) {
                val item = LinkedHashMap<String, Any?>()
                metaData.entries.forEach{
                    item[it.key] = getBySqlType(resultSet, it.key, it.value)
                }
            }
            resultSet.close()
            statement.close()
            return result
        }

        /**
         * @throws SQLException
         */
        @JvmStatic
        private fun getMetaData(metaData: ResultSetMetaData): Map<String, Int> {
            val result = LinkedHashMap<String, Int>(metaData.columnCount, 1f)
            for (i in 1 .. metaData.columnCount) {
                val columnName = checkNotNull(metaData.getColumnName(i - 1))
                result[columnName] = metaData.getColumnType(i - 1)
            }
            return result
        }

        /**
         * @throws SQLException
         */
        @JvmStatic
        private fun getBySqlType(resultSet: ResultSet, columnName: String,
                                 sqlType: Int): Any? {
            return when (sqlType) {
                Types.INTEGER -> resultSet.getInt(columnName)
                Types.VARCHAR, Types.CHAR -> resultSet.getString(columnName)
                Types.SMALLINT -> resultSet.getShort(columnName)
                Types.TINYINT -> resultSet.getByte(columnName)
                Types.DATE, Types.TIME, Types.TIMESTAMP -> resultSet.getDate(columnName)
                else -> resultSet.getObject(columnName)
            }
        }
    }
}