package org.firas.analysis

import javafx.scene.paint.Color

class Colors {

    companion object {
        val values = arrayOf(Color.rgb(0xED, 0x55, 0x65),
                Color.rgb(0xFC, 0x6E, 0x51),
                Color.rgb(0xFF, 0xCE, 0x54),
                Color.rgb(0xA0, 0xD4, 0x68),
                Color.rgb(0x48, 0xCF, 0xAD),
                Color.rgb(0x4F, 0xC1, 0xE9),
                Color.rgb(0x5D, 0x9C, 0xEC),
                Color.rgb(0xAC, 0x92, 0xDC),
                Color.rgb(0xEC, 0x87, 0xC0))
    }
}