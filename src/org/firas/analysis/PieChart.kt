package org.firas.analysis

import javafx.scene.canvas.Canvas
import javafx.scene.shape.ArcType

class PieChart(val portions: Array<Double>) {

    init {
        if (portions.size < 2) {
            throw IllegalArgumentException("Less than 2 portions")
        } else if (portions.size > Colors.values.size) {
            throw IllegalArgumentException("More than " + Colors.values.size + "portions")
        }
        portions.forEach {
            if (it < 0.0) throw IllegalArgumentException("Negative portion")
        }
    }

    val total = portions.sum()

    fun draw(canvas: Canvas): Canvas {
        val context = canvas.graphicsContext2D
        var sum = 90.0
        for (i in 1 .. portions.size) {
            context.fill = Colors.values[i - 1]
            val item = portions[i - 1] * 360 / total
            context.fillArc(50.0, 50.0, 500.0, 500.0, sum, item, ArcType.ROUND)
            sum += item
        }
        return canvas
    }
}