package org.firas.analysis

class Analysis {

    companion object {
        @JvmStatic
        fun average(collection: Collection<Double>): Double {
            if (collection.isEmpty()) {
                throw IllegalArgumentException(
                        "Attempt to calculate the average of an empty collection")
            }
            return collection.sum() / collection.size
        }

        @JvmStatic
        fun squareAverage(collection: Collection<Double>): Double {
            if (collection.isEmpty()) {
                throw IllegalArgumentException(
                        "Attempt to calculate the average of an empty collection")
            }
            return collection.reduce({sum, item -> sum + item}) / collection.size
        }

        @JvmStatic
        fun leastSquare(collection: Collection<Pair<Double, Double>>):
                Triple<Double, Double, Double> {
            if (collection.isEmpty()) {
                throw IllegalArgumentException(
                        "Attempt to calculate the average of an empty collection")
            }
            var xSum = .0
            var ySum = .0
            var xSquareSum = .0
            var xySum = .0
            collection.forEach({
                xSum += it.first
                ySum += it.second
                xSquareSum += it.first * it.first
                xySum += it.first * it.second
            })
            return Triple(xSum / collection.size, ySum / collection.size,
                    (collection.size * xySum - xSum * ySum) /
                    (collection.size * xSquareSum - xSum * xSum))
        }

        @JvmStatic
        fun correlation(collection: Collection<Pair<Double, Double>>): Double {
            if (collection.isEmpty()) {
                throw IllegalArgumentException(
                        "Attempt to calculate the average of an empty collection")
            }
            var xSum = .0
            var ySum = .0
            var xSquareSum = .0
            var ySquareSum = .0
            var xySum = .0
            collection.forEach({
                xSum += it.first
                ySum += it.second
                xSquareSum += it.first * it.first
                ySquareSum += it.second * it.second
                xySum += it.first * it.second
            })
            return (collection.size * xySum - xSum * ySum) /
                    (Math.sqrt(collection.size * xSquareSum - xSum * xSum) *
                            Math.sqrt(collection.size * ySquareSum - ySum * ySum))
        }
    }
}